﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace BSC.Bot.Aurora
{
    //AC: This is from a POC I did on the Nedbank chatbot
    //The code here will be useful in finding staff names in the utternaces
    //Need to rework this accordingly.

    /// <summary>
    /// Finds entities in an utternace using fuzzy matching and corrects them so LUIS can have accurate list entity matching
    /// </summary>
    public class EntityFinder
    {
        //TODO: need to return corrected utterance as well as target that is matched
        public static string PreProcessUtterance(string utterance)
        {
            var tokenizedTargetList = GetTokenizedList(GetEntityList());
            var result = FindEntity(tokenizedTargetList, GetTokenizedString(utterance));
            if (result != null)
            {
                return string.Join(" ", result);
            }
            return null;
        }

        /// <summary>
        /// Finds the first entity appearing in a tokenized utterance
        /// </summary>
        /// <param name="entities">A tokenized list of targets - each target can be more than a single word</param>
        /// <param name="tokenizedUtterance">A tokenized utterance</param>
        /// <returns></returns>
        static string[] FindEntity(List<string[]> entities, string[] tokenizedUtterance)
        {
            string[] foundEntity = null;
            foreach (var entity in entities)
            {
                foundEntity = FindEntity(entity, tokenizedUtterance);
                if (foundEntity != null) { break; }
            }
            return foundEntity;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizedEntity"></param>
        /// <param name="tokenizedUtterance"></param>
        /// <returns></returns>
        static string[] FindEntity(string[] tokenizedEntity, string[] tokenizedUtterance)
        {
            List<int> matchedPositions = new List<int>();

            //Attempt to find first entity token within tokenized utterance
            for (var i = 0; i < tokenizedUtterance.Count(); i++)
            {
                int distanceThreshold = 2;
                if (tokenizedUtterance[i].Length <= 5) { distanceThreshold = 1; }
                if (DLevenshtein.CalcLevenshteinDistance(tokenizedUtterance[i], tokenizedEntity[0]) <= distanceThreshold)
                { matchedPositions.Add(i); }
            }

            //Repeat until list is empty or we have iterated through all entity tokens - i.e. where there is more than one, we need to match all
            for (int curEntityTokenIndex = 1; curEntityTokenIndex < tokenizedEntity.Count(); curEntityTokenIndex++)
            {
                matchedPositions = GetNextMatchingPositions(tokenizedEntity[curEntityTokenIndex], matchedPositions, tokenizedUtterance);
                if (matchedPositions.Count() == 0) { break; }
            }

            if (matchedPositions.Count() > 0)
            {
                //Correct utterance
                for (int i = tokenizedEntity.Length; i > 0; i--)
                {
                    tokenizedUtterance[matchedPositions[0] + i - tokenizedEntity.Length] = tokenizedEntity[i-1];
                }
                return tokenizedUtterance;
            }
            return null;
        }


        /// <summary>
        /// Returns a list of index positions that one token is found within another
        /// </summary>
        /// <param name="find">The token to find</param>
        /// <param name="startingPositions"></param>
        /// <param name="searchWithin">A list of tokens to search within</param>
        /// <returns>A list of matching positions</returns>
        static List<int> GetNextMatchingPositions(string find, List<int> startingPositions, string[] searchWithin)
        {
            List<int> next_matched_positions = new List<int>();
            foreach (int i in startingPositions) //This actually refers to strating tokens...
            {
                if (searchWithin.Count() == i + 1) { break; }
                int distanceThreshold = 2;
                if (find.Length <= 5) { distanceThreshold = 1; }
                if (DLevenshtein.CalcLevenshteinDistance(searchWithin[i + 1], find) <= distanceThreshold)
                { next_matched_positions.Add(i + 1); }
            }

            return next_matched_positions;
        }

        /// <summary>
        /// Returns a list of array tokens
        /// </summary>
        /// <param name="targets"></param>
        /// <returns></returns>
        static List<string[]> GetTokenizedList(List<string> targets)
        {
            var TokenizedList = new List<string[]>();
            foreach (var target in targets)
            {
                TokenizedList.Add(GetTokenizedString(target));
            }
            return TokenizedList;
        }

        /// <summary>
        /// Returns an array of tokens from a string.  Tokenizes based on punctuation and spaces.
        /// </summary>
        /// <param name="target">The string to tokenize</param>
        /// <returns>A string array of tokens</returns>
        static string[] GetTokenizedString(string target)
        {
            return (new string(target.Where(c => !char.IsPunctuation(c)).ToArray())).ToLower().Split();
        }


        static List<string> GetEntityList()
        {
            List<string> entities = new List<string>();
            entities.Add("Abdul Adam");
            entities.Add("Akash Manilal");
            entities.Add("Alexandros Demetriou");
            entities.Add("Andreas Cambitsis");
            entities.Add("Attie Naude");
            entities.Add("Bekezela Ncube");
            entities.Add("Bernice Southey");
            entities.Add("Brandon Pitt");
            entities.Add("Carl Mathapersad");
            entities.Add("Dale Reibeling");
            entities.Add("Daniel Diedericks");
            entities.Add("Darren Cohen");
            entities.Add("David Moore");
            entities.Add("David Fisher");
            entities.Add("David Turner");
            entities.Add("Dhaneshri Pillay");
            entities.Add("Elaine Sarran");
            entities.Add("Elton Bondi");
            entities.Add("Estella Boshoff");
            entities.Add("Evelyn Mashaba");
            entities.Add("Friedrich Burkhard von der Osten");
            entities.Add("Gary Cohen");
            entities.Add("Gavin Mannion");
            entities.Add("George Krafft");
            entities.Add("Gordon Craig");
            entities.Add("Guy Moorcroft");
            entities.Add("Hayley Grover");
            entities.Add("Hung Tran");
            entities.Add("Ilze Du Plooy");
            entities.Add("Jaco Saunders");
            entities.Add("Jacques Louw");
            entities.Add("Jeanette Ngcobo");
            entities.Add("Jessica O'Brien");
            entities.Add("Joel Turner");
            entities.Add("Jonathan Sevenoaks");
            entities.Add("Kgotso Mathe");
            entities.Add("Krati Bogiages");
            entities.Add("Louis Van Der Merwe");
            entities.Add("Marco Luizinho");
            entities.Add("Maria Monyongula");
            entities.Add("Marianne Du Preez");
            entities.Add("Marinus Krommenhoek");
            entities.Add("Mathews Chaitezwi");
            entities.Add("Michael Constantinou");
            entities.Add("Michael Theodorou");
            entities.Add("Michail Scholiadis");
            entities.Add("Mirriam Baartman");
            entities.Add("Nardus Pretorius");
            entities.Add("Neil Davis");
            entities.Add("Nicholas Petrou");
            entities.Add("Nicholas Harvey");
            entities.Add("Nicola Zidel");
            entities.Add("Onesmus Nyakumbi");
            entities.Add("Paul Dos Santos");
            entities.Add("Purpose Katakwa");
            entities.Add("Righardt Du Plessis");
            entities.Add("Robert Malan");
            entities.Add("Roelof Schoeman");
            entities.Add("Ross Adams");
            entities.Add("Sean Mowatt");
            entities.Add("Sebastien Gruber");
            entities.Add("Sergio Mendes");
            entities.Add("Shameel Nana");
            entities.Add("Shashank Venkatesh");
            entities.Add("Sipho Khumalo");
            entities.Add("Tapiwa Bosha");
            entities.Add("Tebogo Marope");
            entities.Add("Thulani Makoba");
            entities.Add("Tony Savides");
            entities.Add("Trevor Kuhlmann");
            entities.Add("Tumi Manana");
            entities.Add("Veronica Munyongani");
            entities.Add("Vikesh Perumal");
            entities.Add("Vinay Varghese");
            entities.Add("Willie De Beer");
            entities.Add("Zander Fick");
            entities.Add("Abdul");
            entities.Add("Akash");
            entities.Add("Alexandros");
            entities.Add("Andreas");
            entities.Add("Attie");
            entities.Add("Bekezela");
            entities.Add("Bernice");
            entities.Add("Brandon");
            entities.Add("Carl");
            entities.Add("Dale");
            entities.Add("Daniel");
            entities.Add("Darren");
            entities.Add("David");
            entities.Add("David");
            entities.Add("David");
            entities.Add("Dhaneshri");
            entities.Add("Elaine");
            entities.Add("Elton");
            entities.Add("Estella");
            entities.Add("Evelyn");
            entities.Add("Friedrich Burkhard");
            entities.Add("Gary");
            entities.Add("Gavin");
            entities.Add("George");
            entities.Add("Gordon");
            entities.Add("Guy");
            entities.Add("Hayley");
            entities.Add("Hung");
            entities.Add("Ilze");
            entities.Add("Jaco");
            entities.Add("Jacques");
            entities.Add("Jeanette");
            entities.Add("Jessica");
            entities.Add("Joel");
            entities.Add("Jonathan");
            entities.Add("Kgotso");
            entities.Add("Krati");
            entities.Add("Louis");
            entities.Add("Marco");
            entities.Add("Maria");
            entities.Add("Marianne");
            entities.Add("Marinus");
            entities.Add("Mathews");
            entities.Add("Michael");
            entities.Add("Michael");
            entities.Add("Michail");
            entities.Add("Mirriam");
            entities.Add("Nardus");
            entities.Add("Neil");
            entities.Add("Nicholas");
            entities.Add("Nicholas");
            entities.Add("Nicola");
            entities.Add("Onesmus");
            entities.Add("Paul");
            entities.Add("Purpose");
            entities.Add("Righardt");
            entities.Add("Robert");
            entities.Add("Roelof");
            entities.Add("Ross");
            entities.Add("Sean");
            entities.Add("Sebastien");
            entities.Add("Sergio");
            entities.Add("Shameel");
            entities.Add("Shashank");
            entities.Add("Sipho");
            entities.Add("Tapiwa");
            entities.Add("Tebogo");
            entities.Add("Thulani");
            entities.Add("Tony");
            entities.Add("Trevor");
            entities.Add("Tumi");
            entities.Add("Veronica");
            entities.Add("Vikesh");
            entities.Add("Vinay");
            entities.Add("Willie");
            entities.Add("Zander");
            return entities;
        }
    }


    /// <summary>
    /// Class that makes all API calls to PeopleHR
    /// </summary>
    class PeopleHRAPI
    {
        ///Get skills for employee
        public static List<JToken[]> GetEmployeeSkills(string skillsType)
        {
            /* Allowable QueryNames are:
             *      zSkillsAnalyticalByEmployee_AC 
             *      zSkillsIndustryByEmployee_AC
             *      zSkillsOtherByEmployee_AC
             *      zSkillsTechByEmployee_AC    */

            //TODO: hardcoded for test purposes - need to pass through to the function one of the above values.
            //skillsType = "zSkillsAnalyticalByEmployee_AC";
            List<JToken[]> skills = new List<JToken[]>();
            string responseText = PeopleHRAPI.MakeJSONRequest("https://peoplehr.azurewebsites.net/api/GetEmployeeSkills?code=RJnYotaNMpAGPJ0k4GaLf2q7yDZp4ySI1jjAICottxeNOWFlHU1bjQ==", "{\"QueryName\": \"" + skillsType + "\"}");
            var obj = JObject.Parse(responseText);
            foreach (var row in obj["Result"].Cast<JObject>())
            {
                var values = row.PropertyValues().ToArray();
                skills.Add(values);
            }
            return skills;
        }

        ///Get employee object from employee code
        public static dynamic GetEmployeeByCode(string employeeCode)
        {
            if (string.IsNullOrEmpty(employeeCode)) return null;
            string responseText = PeopleHRAPI.MakeJSONRequest("https://api.peoplehr.net/Employee", "{\"APIKey\":\"6725e05f-84ec-43ff-a5aa-668795dbed48\", \"Action\": \"GetEmployeeDetailById\", \"EmployeeId\": \"" + employeeCode + "\"}");
            dynamic employee = JsonConvert.DeserializeObject(responseText);
            return employee.Result;
        }

        ///Gets employee code given a Full Name
        public static Dictionary<string, string> FindEmployeeInPeopleHR(Tuple<string, string> SearchName)
        {
            //This allows us to make a call to PeopleHR API
            //TODO: Make this smarter to use surname as well if provided

            //This api call returns an error, need to investigate
            //dynamic data = PeopleHRAPI.MakeJSONRequest("https://api.peoplehr.net/Query", "{\"APIKey\":\"6725e05f-84ec-43ff-a5aa-668795dbed48\", \"Action\": \"GetQueryResultByQueryName\", \"QueryName\": \"zEmployeeIDs_AC\"}");
            string responseText = PeopleHRAPI.MakeJSONRequest("https://peoplehr.azurewebsites.net/api/GetEmployeeCodes?code=RJnYotaNMpAGPJ0k4GaLf2q7yDZp4ySI1jjAICottxeNOWFlHU1bjQ==", "");
            dynamic data = JsonConvert.DeserializeObject(responseText);
            string employeeCode = "";
            string firstName = "";
            string lastName = "";
            int maxLevDist = 4;
            foreach (var x in data.Result)
            {
                int levDist = DLevenshtein.CalcLevenshteinDistance(x["First Name"].Value, SearchName.Item1);
                if (levDist <= maxLevDist)
                {
                    maxLevDist = levDist - 1; //Make target for next match one edit closer
                    employeeCode = x["Employee Id"];
                    firstName = x["First Name"];
                    lastName = x["Last Name"];
                    if (levDist == 0) //If exact match then no need to search further
                    {
                        break;
                    }
                }
            }
            Dictionary<string, string> employee = new Dictionary<string, string>();
            employee.Add("Employee ID", employeeCode);
            employee.Add("First Name", firstName);
            employee.Add("Last Name", lastName);
            return employee;
        }

        public static List<JToken[]> GetSkillsByEmployeeCode(string employeeCode)
        {
            List<JToken[]> skills = GetSkills();
            return skills.Where(a => (a[0].ToString() == employeeCode))
                .OrderByDescending(a => a[5].ToString()).ToList();
        }

        public static List<JToken[]> GetEmployeesWithSkill(string skill)
        {
            List<JToken[]> skills = GetSkills();
            return skills.Where(a => (a[4].ToString() == skill))
                .OrderByDescending(a => a[5].ToString()).ToList(); //a[5]
        }

        private static List<JToken[]> GetSkills()
        {
            List<JToken[]> results = new List<JToken[]>();
            GetSkillsByEmployeeCode(ref results, "zSkillsTechByEmployee_AC");
            GetSkillsByEmployeeCode(ref results, "zSkillsAnalyticalByEmployee_AC");
            GetSkillsByEmployeeCode(ref results, "zSkillsIndustryByEmployee_AC");
            GetSkillsByEmployeeCode(ref results, "zSkillsOtherByEmployee_AC");

            return results;
        }

        private static void GetSkillsByEmployeeCode(ref List<JToken[]> results, string skillsType)
        {
            List<JToken[]> skills = PeopleHRAPI.GetEmployeeSkills(skillsType);
            results.AddRange(skills);
        }


        ///Helper function to make calls to PeopleHR API
        public static string MakeJSONRequest(string url, string body)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "text/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(body);
            }

            //dynamic data;
            string responseText;
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                responseText = streamReader.ReadToEnd();
                //data = JsonConvert.DeserializeObject(responseText);
            }
            return responseText;
        }
    }

    class AllocationsAPI
    {
        //Gets allocations for specific employee


    }

    static class DLevenshtein
    {
        public static int CalcLevenshteinDistance(string original, string modified)
        {
            if (original == modified)
                return 0;

            int len_orig = original.Length;
            int len_diff = modified.Length;

            if (len_orig == 0)
                return len_diff;

            if (len_diff == 0)
                return len_orig;

            var matrix = new int[len_orig + 1, len_diff + 1];

            for (int i = 1; i <= len_orig; i++)
            {
                matrix[i, 0] = i;
                for (int j = 1; j <= len_diff; j++)
                {
                    int cost = modified[j - 1] == original[i - 1] ? 0 : 1;
                    if (i == 1)
                        matrix[0, j] = j;

                    var vals = new int[] {
                    matrix[i - 1, j] + 1,
                    matrix[i, j - 1] + 1,
                    matrix[i - 1, j - 1] + cost
                };
                    matrix[i, j] = vals.Min();
                    if (i > 1 && j > 1 && original[i - 1] == modified[j - 2] && original[i - 2] == modified[j - 1])
                        matrix[i, j] = Math.Min(matrix[i, j], matrix[i - 2, j - 2] + cost);
                }
            }
            return matrix[len_orig, len_diff];
        }
    }
}

