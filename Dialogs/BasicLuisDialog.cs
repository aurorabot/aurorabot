using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using BSC.Bot.Aurora;



using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace BSC.Bot.Aurora
{
    // For more information about this template visit http://aka.ms/azurebots-csharp-luis
    [Serializable]
    public class BasicLuisDialog : LuisDialog<object>
    {

        public const string Entity_StaffName = "HRBot.StaffName";
        public const string Entity_Skills = "HRBot.Skills";
                
        public BasicLuisDialog() : base(new LuisService(new LuisModelAttribute(
            ConfigurationManager.AppSettings["LuisAppId"], 
            ConfigurationManager.AppSettings["LuisAPIKey"], 
            domain: ConfigurationManager.AppSettings["LuisAPIHostName"])))
        {
        }

        [LuisIntent("None")]
        public async Task NoneIntent(IDialogContext context, LuisResult result)
        {
            await this.ShowLuisResult(context, result);
        }

        // Go to https://luis.ai and create a new intent, then train/publish your luis app.
        // Finally replace "Gretting" with the name of your newly created intent in the following handler
        [LuisIntent("Greeting")]
        public async Task GreetingIntent(IDialogContext context, LuisResult result)
        {
            await context.PostAsync(text: "Hi! I'm Aurora, the HR Bot. How can I help you? \U0001F600");
            context.Wait(this.MessageReceived);
        }

        [LuisIntent("Cancel")]
        public async Task CancelIntent(IDialogContext context, LuisResult result)
        {
            await this.ShowLuisResult(context, result);
        }

        [LuisIntent("Help")]
        public async Task HelpIntent(IDialogContext context, LuisResult result)
        {
            await context.PostAsync(text: "Looks like you need some help. I can help you find information about your colleagues such as their skills, tenure, phone number and career manager. "
                + "You can start by asking something like 'who is John Smith's career manager' and I'll be able to help you... \U0001F600");
            context.Wait(this.MessageReceived);
        }

        [LuisIntent("About")]
        public async Task AboutIntent(IDialogContext context, LuisResult result)
        {
            await context.PostAsync(text: "I'm Aurora, the HR Bot. I can help you find information about your colleagues such as their skills, tenure, phone number and career manager. "
                + "Just ask me a question and I'll try my best to help... \U0001F600");
            context.Wait(this.MessageReceived);
        }

        [LuisIntent("TellMeAboutEmployee")]
        public async Task AboutEmployeeIntent(IDialogContext context, LuisResult result)
        {
            string name = this.BotEntityRecognition("HRBot.TellMeAboutEmployee", result);

            char[] splitchar = { ' ' };
            string[] fullname = name.Split(splitchar);
            string firstname = fullname[0];
            string surname = fullname[1];
            var EmployeeName = new Tuple<string, string>(firstname, surname);

          
            //TODO: remove comment - Shameel this now will correctly get the Employee ID given an employee name (it will work if only the first name is provided)
            var employeeCode = PeopleHRAPI.FindEmployeeInPeopleHR(EmployeeName)["Employee ID"];

            dynamic employee = PeopleHRAPI.GetEmployeeByCode(employeeCode);

            List<JToken[]> skills = PeopleHRAPI.GetSkillsByEmployeeCode(employee.EmployeeId.DisplayValue.ToString());
            var skillsDisplay = string.Join(", ", skills.Select(x => x[4]));

            string Gender = employee.Gender.DisplayValue;
            string HisHer = "";

            if (Gender == "Male")
            {
                HisHer = "His";
            }
            else
            {
                HisHer = "Her";
            }

            await context.PostAsync($"{employee.FirstName.DisplayValue} {employee.LastName.DisplayValue} has the role of {employee.JobRole.DisplayValue}. " +
                $"{employee.FirstName.DisplayValue} started with the firm on {employee.StartDate.DisplayValue}. " + HisHer + $" career manager is {employee.ReportsTo.DisplayValue}.");

            
            context.Wait(MessageReceived);
        }

        [LuisIntent("GetTenure")]
        public async Task TenureIntent(IDialogContext context, LuisResult result)
        {
            string name = this.BotEntityRecognition("HRBot.GetTenure", result);

            char[] splitchar = { ' ' };
            string[] fullname = name.Split(splitchar);
            string firstname = fullname[0];
            string surname = fullname[1];
            var EmployeeName = new Tuple<string, string>(firstname, surname);

            var employeeCode = PeopleHRAPI.FindEmployeeInPeopleHR(EmployeeName)["Employee ID"];

            dynamic employee = PeopleHRAPI.GetEmployeeByCode(employeeCode);

            await context.PostAsync($"{employee.FirstName.DisplayValue} {employee.LastName.DisplayValue} started on {employee.StartDate.DisplayValue}.");
            context.Wait(MessageReceived);
        }

        [LuisIntent("GetRole")]
        public async Task RoleIntent(IDialogContext context, LuisResult result)
        {
            string name = this.BotEntityRecognition("HRBot.GetRole", result);

            char[] splitchar = { ' ' };
            string[] fullname = name.Split(splitchar);
            string firstname = fullname[0];
            string surname = fullname[1];
            var EmployeeName = new Tuple<string, string>(firstname, surname);

            var employeeCode = PeopleHRAPI.FindEmployeeInPeopleHR(EmployeeName)["Employee ID"];

            dynamic employee = PeopleHRAPI.GetEmployeeByCode(employeeCode);

            await context.PostAsync($"{employee.FirstName.DisplayValue} {employee.LastName.DisplayValue} has the role of {employee.JobRole.DisplayValue}.");
            context.Wait(MessageReceived);
        }

        [LuisIntent("GetPhoneNumber")]
        public async Task PhoneIntent(IDialogContext context, LuisResult result)
        {
            string name = this.BotEntityRecognition("HRBot.GetPhoneNumber", result);

            char[] splitchar = { ' ' };
            string[] fullname = name.Split(splitchar);
            string firstname = fullname[0];
            string surname = fullname[1];
            var EmployeeName = new Tuple<string, string>(firstname, surname);

            var employeeCode = PeopleHRAPI.FindEmployeeInPeopleHR(EmployeeName)["Employee ID"];

            dynamic employee = PeopleHRAPI.GetEmployeeByCode(employeeCode);

            await context.PostAsync($"{employee.FirstName.DisplayValue} {employee.LastName.DisplayValue}'s mobile number is {employee.ContactDetail.Mobile.DisplayValue}.");
            context.Wait(MessageReceived);
        }

        [LuisIntent("FindEmployeesWithSkills")]
        public async Task EmployeesWithSkillsIntent(IDialogContext context, LuisResult result)
        {
            string skill = this.BotSklillsEntityRecognition("HRBot.FindEmployeesWithSkills", result);

            dynamic employeenames = PeopleHRAPI.GetEmployeesWithSkill(skill);
            
            await context.PostAsync($"The following people have {skill} skill's.{FormatEmployeesForSkill(employeenames)}.");

            context.Wait(MessageReceived);
        }

        [LuisIntent("CurrentAllocation")]
        public async Task CurrentAlllocationIntent(IDialogContext context, LuisResult result)
        {
            string name = this.BotEntityRecognition("HRBot.CurrentAllocation", result);

            char[] splitchar = { ' ' };
            string[] fullname = name.Split(splitchar);
            string firstname = fullname[0];
            string surname = fullname[1];
            var EmployeeName = new Tuple<string, string>(firstname, surname);

            var employeeCode = PeopleHRAPI.FindEmployeeInPeopleHR(EmployeeName)["Employee ID"];

            dynamic employee = PeopleHRAPI.GetEmployeeByCode(employeeCode);

            await context.PostAsync($"I understand you want to find the allocation for {employee.FirstName.DisplayValue} {employee.LastName.DisplayValue}. I cannot assist you with that at the moment, but I am learning so I'll be able to assist you soon... \U0001F600");
            context.Wait(MessageReceived);
        }

        [LuisIntent("EmployeeOnLeave")]
        public async Task EmployeeOnLEaveIntent(IDialogContext context, LuisResult result)
        {
            string name = this.BotEntityRecognition("HRBot.EmployeeOnLeave", result);

            char[] splitchar = { ' ' };
            string[] fullname = name.Split(splitchar);
            string firstname = fullname[0];
            string surname = fullname[1];
            var EmployeeName = new Tuple<string, string>(firstname, surname);

            var employeeCode = PeopleHRAPI.FindEmployeeInPeopleHR(EmployeeName)["Employee ID"];

            dynamic employee = PeopleHRAPI.GetEmployeeByCode(employeeCode);

            await context.PostAsync($"I understand you want to find out about {employee.FirstName.DisplayValue} {employee.LastName.DisplayValue}'s leave. I cannot assist you with that at the moment, but I am learning so I'll be able to assist you soon... \U0001F600");
            context.Wait(MessageReceived);
        }

        [LuisIntent("FindCareerManager")]
        public async Task CareerManagerIntent(IDialogContext context, LuisResult result)
        {
            string name = this.BotEntityRecognition("HRBot.FindCareerManager", result);

            char[] splitchar = { ' ' };
            string[] fullname = name.Split(splitchar);
            string firstname = fullname[0];
            string surname = fullname[1];
            var EmployeeName = new Tuple<string, string>(firstname, surname);

            var employeeCode = PeopleHRAPI.FindEmployeeInPeopleHR(EmployeeName)["Employee ID"];

            dynamic employee = PeopleHRAPI.GetEmployeeByCode(employeeCode);

            await context.PostAsync($"{employee.FirstName.DisplayValue} {employee.LastName.DisplayValue}'s career manager is {employee.ReportsTo.DisplayValue}.");
            context.Wait(MessageReceived);
        }

        [LuisIntent("GetSkills")]
        public async Task SkillsIntent(IDialogContext context, LuisResult result)
        {
            string name = this.BotEntityRecognition("HRBot.GetSkills", result);

            char[] splitchar = { ' ' };
            string[] fullname = name.Split(splitchar);
            string firstname = fullname[0];
            string surname = fullname[1];
            var EmployeeName = new Tuple<string, string>(firstname, surname);

            var employeeCode = PeopleHRAPI.FindEmployeeInPeopleHR(EmployeeName)["Employee ID"];

            dynamic employee = PeopleHRAPI.GetEmployeeByCode(employeeCode);

            dynamic skills = PeopleHRAPI.GetSkillsByEmployeeCode(employeeCode);

            await context.PostAsync($"{employee.FirstName.DisplayValue} {employee.LastName.DisplayValue} has the following skills, {FormatEmployeeSkills(skills)}.");
            context.Wait(MessageReceived);
        }

        [LuisIntent("EmployeeAvailability")]
        public async Task EmployeeAvailabilityIntent(IDialogContext context, LuisResult result)
        {
            string name = this.BotEntityRecognition("HRBot.EmployeeAvailability", result);

            char[] splitchar = { ' ' };
            string[] fullname = name.Split(splitchar);
            string firstname = fullname[0];
            string surname = fullname[1];
            var EmployeeName = new Tuple<string, string>(firstname, surname);

            var employeeCode = PeopleHRAPI.FindEmployeeInPeopleHR(EmployeeName)["Employee ID"];

            dynamic employee = PeopleHRAPI.GetEmployeeByCode(employeeCode);

            await context.PostAsync($"I understand you want to find out about {employee.FirstName.DisplayValue} {employee.LastName.DisplayValue}'s availability. I cannot assist you with that at the moment, but I am learning so I'll be able to assist you soon... \U0001F600");
            context.Wait(MessageReceived);
        }

        [LuisIntent("EmployeeBackFromLeave")]
        public async Task EmployeeBackFromLeaveIntent(IDialogContext context, LuisResult result)
        {
            string name = this.BotEntityRecognition("HRBot.EmployeeBackFromLeave", result);

            char[] splitchar = { ' ' };
            string[] fullname = name.Split(splitchar);
            string firstname = fullname[0];
            string surname = fullname[1];
            var EmployeeName = new Tuple<string, string>(firstname, surname);

            var employeeCode = PeopleHRAPI.FindEmployeeInPeopleHR(EmployeeName)["Employee ID"];

            dynamic employee = PeopleHRAPI.GetEmployeeByCode(employeeCode);

            await context.PostAsync($"I understand you want to find out when {employee.FirstName.DisplayValue} {employee.LastName.DisplayValue} is back from leave. I cannot assist you with that at the moment, but I am learning so I'll be able to assist you soon... \U0001F600");
            context.Wait(MessageReceived);
        }

        [LuisIntent("EmployeePlannedLeave")]
        public async Task EmployeePlannedLeaveIntent(IDialogContext context, LuisResult result)
        {
            string name = this.BotEntityRecognition("HRBot.EmployeePlannedLeave", result);

            char[] splitchar = { ' ' };
            string[] fullname = name.Split(splitchar);
            string firstname = fullname[0];
            string surname = fullname[1];
            var EmployeeName = new Tuple<string, string>(firstname, surname);

            var employeeCode = PeopleHRAPI.FindEmployeeInPeopleHR(EmployeeName)["Employee ID"];

            dynamic employee = PeopleHRAPI.GetEmployeeByCode(employeeCode);

            await context.PostAsync($"I understand you want to find out about {employee.FirstName.DisplayValue} {employee.LastName.DisplayValue}'s planned leave. I cannot assist you with that at the moment, but I am learning so I'll be able to assist you soon... \U0001F600");
            context.Wait(MessageReceived);
        }

        [LuisIntent("ProjectAllocation")]
        public async Task ProjectAllocationIntent(IDialogContext context, LuisResult result)
        {
            //string name = this.BotEntityRecognition("HRBot.ProjectAllocation", result);

            await context.PostAsync($"I understand you want to find out who is allocated to a specific project. I cannot assist you with that at the moment, but I am learning so I'll be able to assist you soon... \U0001F600");
            context.Wait(MessageReceived);
        }

        private async Task ShowLuisResult(IDialogContext context, LuisResult result) 
        {
            await context.PostAsync($"You have reached {result.Intents[0].Intent}. You said: {result.Query}");
            context.Wait(MessageReceived);
        }



        ///Entity recognition
        public string BotEntityRecognition(string intentName, LuisResult result)
        {
            string fullname = "";

            foreach (var entity in result.Entities)
            {
                var dict = entity.Resolution.Values.GetEnumerator();
                dict.MoveNext();
                var valuesList = (List<object>)dict.Current;
                var canonicalForm = (string)valuesList[0];
                fullname = canonicalForm.ToString();
            }
            
            return fullname;
        }

        public string BotSklillsEntityRecognition(string intentName, LuisResult result)
        {
            string skill = "";

            foreach (var entity in result.Entities)
            {
                var dict = entity.Resolution.Values.GetEnumerator();
                dict.MoveNext();
                var valuesList = (List<object>)dict.Current;
                var canonicalForm = (string)valuesList[0];
                skill = canonicalForm.ToString();
            }

            return skill;
        }


        ///Format role returned from PeopleHR into more user friendly format
        private static string GetCleanRole(string role)
        {
            int removeIndex = role.IndexOf("-");
            if (removeIndex > 0) { return role.Remove(role.IndexOf("-") - 1); }
            else { return role; }
        }

        
        private static string FormatEmployeeSkills(List<JToken[]> skills)
        {
            string level = "";
            string result = "";
            foreach (JToken[] skill in skills)
            {
                if (level == skill[5].ToString())
                {
                    result += $"{skill[4].ToString()}, ";
                }
                else
                {
                    level = skill[5].ToString();
                    result += $"{Environment.NewLine}level {level}: {skill[4].ToString()}, ";
                }
            }
            int stringlength = result.Length;
            result = result.Remove(stringlength - 2, 2);
            return result;
        }

        
        private static string FormatEmployeesForSkill(List<JToken[]> employeeSkills)
        {
            string level = "";
            string result = "";
            foreach (JToken[] employee in employeeSkills)
            {
                if (level == employee[5].ToString())
                {
                    result += $"{employee[1].ToString()} {employee[2].ToString()}, ";
                }
                else
                {
                    level = employee[5].ToString();
                    result += $"\n{Environment.NewLine}Level {level}: {employee[1].ToString()} {employee[2].ToString()}, ";
                }
            }
            result = result.Remove(result.Length - 2);
            return result;
        }


        private static Tuple<string, string> GetNameAndSurnameFromIntent(dynamic data)
        {
            string Firstname = data.result.parameters["given-name"].Value;

            //string Firstname = BotEntityRecognition("HRBot.TellMeAboutEmployee", result);

            string Surname = "";// data.result.parameters["last-name"].Value;
            var FullName = new Tuple<string, string>(Firstname, Surname);

            return FullName;
        }
    }
}
